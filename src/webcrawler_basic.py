import requests
import re

url='https://oer-informatik.gitlab.io/python-basics/webcrawler/design15/index.html'

session = requests.Session()
response = session.get(url, allow_redirects=True)
html_sourcecode = ""

if response.status_code != 200:
    print("Error fetching page:")
    print(response)
    exit()
else:
    html_sourcecode = response.content

# print(html_sourcecode)

for regtext in re.findall('[a-z]+@[a-z]+', str(html_sourcecode)):
    print(regtext)

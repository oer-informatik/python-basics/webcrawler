## Einen Webcrawler in Python programmieren

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110451036309748716</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/python_webcrawler</span>


> **tl/dr;** _(ca. 10 min Lesezeit): Es soll ein Webcrawler programmiert werden, der den eigenen Internetauftritt nach dort veröffentlichten E-Mail-Adressen durchsucht. Für die betroffenen Besitzer der E-Mail-Konten sollen so Awareness-Trainings angeboten werden._

### _Social Engineering_ bei SPAM und Ransomware

_Social engineering_-Angriffe nutzen Neugierde und vermeintliche Vertrauensstellungen, um sich Zugang zu Systemen zu verschaffen. Ein vor der Bürotür ausgelegter USB-Stick erweckt Neugierde: Was mag darauf sein? Es besteht eine gewisse Chance, dass die Neugierde größer ist als die Vorsicht - und wider besseren Wissens der Stick eingesteckt wird.

![Ein irgendwie entsetzter Roboter neben einem Serverschrank. Offensichtlich ist etwas schreckliches passiert...](images/robotserverrack.png)

Ein anderer Angriffsvektor ist das Vorgaukeln von Vertrauensstellungen. Bei einer Mail, deren Absender vermeintlich eine Kollegin oder ein Kollege ist, ist die Chance größer, dass ein Anhang geöffnet wird, ein gesendetes Archiv entpackt oder Makros ausgeführt werden. Auf diesem Weg findet Schadcode seinen Weg in Unternehmensnetze. Auch Ransomware wie der Erpressungstrojaner Locky nutzt derartige Einfallstore als ersten Schritt.

Dazu werden durch die Angreifer jedoch Informationen benötigt, die beispielsweise auch öffentlich auf der Internetseite des eigenen Unternehmens preisgegeben werden: wenn auf der Internetseite zwei Namen (oder gar zwei E-Mail-Adressen) veröffentlicht sind, die miteinander in Bezug stehen, ist die Wahrscheinlichkeit groß, dass eine Mail des einen Kontos vom anderen Konto geöffnet wird - und zwar ohne die Vorsichtsmaßnahmen, die bei unbekannten Absendern greifen würden.

![Der Versuch von DALL-E, einen Datenklau am PC darzustellen (einer greift einem Computernutzer über die Schultern in die Tastatur)](images/pchijacker.png)

### Webcrawler zur Analyse des _E-Mail-Harvesting_

Hier soll der Webcrawler bei der Gefährdungsbeurteilung helfen: Analysiert werden soll die Anfälligkeit des Unternehmensauftritts im Internet für das gezielte automatisierte Sammeln von E-Mail-Adressen, die miteinander in Kontakt stehen (_E-Mail-Harvesting_).

Hierzu soll ein Python-Tool entwickelt werden. Dieses Tool soll alle E-Mail-Adressen, die auf den Internetseiten eines Unternehmens veröffentlicht werden, auflisten.

Dieser Webcrawler soll 

-	nach Eingabe einer Start-URL selbständig die E-Mail-Adressen auf der jeweiligen Seite finden,

-	in einer zweiten Iteration alle direkt verlinkten Seiten einer Domain untersuchen,

-	in einer dritten Iteration alle verlinkten Seiten der Domain bis zu einer einzugebenden Tiefe an Ebenen durchsuchen.

Dabei soll er zu jeder gefundenen E-Mail-Adresse alle URLs, auf denen sie gefunden wurde, speichern. 

Das Programm soll zwei Berichte als Textdatei ins Dateisystem ausgeben können:

- die gefundenen E-Mail-Adressen alphabetisch sortiert mit zugehörigen Links und

- die gefundenen E-Mail-Adressen und deren Häufigkeit entsprechend der Häufigkeit.

Bericht 1:
```
anton.mustermann@beispieldomain.de
 - http://www.beispieldomain.de/hierauch
 - http://www.beispieldomain.de/hierauchnochmal
musterfrau@beispieldomain.de
 - http://www.beispieldomain.de/hier
 - http://www.beispieldomain.de/hierauch
 - http://www.beispieldomain.de/hierauch
```

Bericht 2:
```
musterfrau@beispieldomain.de (3)
anton.mustermann@beispieldomain.de (2)
```

Es soll sichergestellt werden, dass jede Seite nur einmal untersucht wird.

Mithilfe der so gewonnenen Liste soll später die Unternehmensleitung entscheiden können, ob die betroffenen Adressen veröffentlicht werden müssen und ob weitere Maßnahmen (Sensibilisierung der Mitarbeiter / Kunden, E-Mail-Empfang in isolierten Netzwerken) erforderlich sind.

![Ein Roboter liest ein Buch. Eigentlich soll es der Webcrawler sein, aber DALL-E hat mich nicht verstanden...](images/webcrawler.png)


### Disclaimer

Webcrawler produzieren Traffic, verarbeiten und speichern Daten, produzieren letztlich $CO_2$. Sie sollten also nicht aus _Jux und Dollerei_ ohne konkreten Anlass laufen. 

Wenn Webcrawler systematisch Internetseiten durchsuchen, öffnen sie alle Unterseiten einer Webpage. Sie verhalten sich wie ein Clickmonkey, der wild auf alles klickt, was sich auf der Seite befindet. Das kann zweierlei folgen haben:

![Ein Affe an einer Tastatur. Der Clickmonkey klickt auf alles, was ihm in den Weg kommt.](images/clickmonkey.png)

- Das htt-Protokoll sieht vor, dass `GET`-Aufrufe (das sind Klicks auf Links in der Regel) keinerlei Seiteneffekte haben dürfen. Sie müssen also _safe_ sein. Gegen diese Protokollregel könnten Programmierer aber auch verstoßen haben und ggf. Aktionen wie "Lösche alle Kundendaten" mit einem `GET`-Request auslösen. Zugegeben: Ein unrealistisches drastisches Beispiel, aber eben nicht unmöglich. Wenn derartig großzügig ausgelegte Standards im Unternehmen vorkommen können, sollte man den Crawler nicht auf die "echte" Seite loslassen.

- Webcrawler verursachen Traffic auf dem Server, werden im Serverlog registriert und von Tools gegebenenfalls als verdächtiges Verhalten interpretiert. Manche Admins stufen Crawler als unerwünschten Traffic ein und sperren den Zugang der Crawler (z.B. über dessen IP-Adresse). Gegebenenfalls ist dann der Webauftritt von diesem Rechner oder aus diesem Subnetz nicht mehr erreichbar.

Webauftritte enthalten in der Regel eine `robots.txt`-Datei, in der festgeschrieben ist, wer ("User-Agent") welche Verzeichnisse durchsuchen darf. Das Auswerten dieser Datei belassen wir zunächst den Fortgeschrittenen. Damit wir die Regeln der jeweiligen `robots.txt` nicht brechen, habe ich unter `https://oer-informatik.gitlab.io/python-basics/webcrawler/design15/index.html` eine kleine Internetseite aus einem Template erstellt, ein paar Unterseiten und viele Mailadressen hinterlegt. Diese Adresse kann als Ausgangspunkt für erste Gehversuche dienen.

### Ausgangspunkt: Codebeispiel

Ausgangspunkt der Entwicklung kann das folgende Stück Code sein. Mithilfe der Bibliothek `requests` (ggf. per `python -m pip install requests` nachladen) wird der HTML-Quelltext von Webpages ausgelesen.

Die Bibliothek `re` wiederum sorgt mit _Regular Expressions_ dafür, dass bestimmte Muster erkannt werden (E-Mail-Adressen, Links zu Unterseiten). Der Ausdruck `[a-z]+@[a-z]+` soll ein Ausgangspunkt dafür sein, E-Mail-Adressen im HTML-Quelltext zu finden. Natürlich funktioniert das Programm noch nicht korrekt, stellt aber die Techniken vor, die wir benötigen:

```python
import requests
import re

url='https://oer-informatik.gitlab.io/python-basics/webcrawler/design15/index.html'

session = requests.Session()
response = session.get(url, allow_redirects=True)
html_sourcecode = ""

if response.status_code != 200:
    print("Error fetching page:")
    print(response)
    exit()
else:
    html_sourcecode = response.content

# print(html_sourcecode)

for regtext in re.findall('[a-z]+@[a-z]+', str(html_sourcecode)):
    print(regtext)
```

### Realisierung

Die Entwicklung soll in 3 Iterationsschritten ablaufen: zum jeweiligen Unterrichtsende sollen jeweils Prototypen vorliegen, die bereits einen (selbstgewählten) Teil des Funktionsumfangs umsetzen.

1. Gliedert den Programmablauf zunächst, in dem ihr Klassen und Methoden definieren, die bestimmte Aufgaben (_Operations_) des Webcrawlers übernehmen. Ruft diese Methoden als Abfolgen in einer Methode (_Integration_) auf, ohne die einzelnen Methoden bereits implementiert zu haben (siehe _[Integration Operation Segregation Principle (IOSP)](https://clean-code-developer.de/die-grade/roter-grad/#Integration_Operation_Segregation_Principle_IOSP)_).

2. Erstellt einfache Tests für diese Methoden mit [pytest](https://oer-informatik.de/python_einstieg_pytest)

3. Dokumentiert das Projekt in ihrem _git_-Repository mit einer Aussagekräftigen [Readme-Datei](https://oer-informatik.de/git04-readme-erstellen)

4. Erweiterung 1: Sofern die oben genannten Anforderungen bereits umgesetzt wurden ergänzt eine Ausnahmen- und Fehlerbehandlung in das Programm.

5. Erweiterung 2: Webauftritte regeln mit einer `robots.txt`-Datei in der Regel, welche Bereiche von Crawler durchsucht werden dürfen. Informiert euch über diesen Mechanismus (z.B. über die entsprechende Seite von [SelfHTML](https://wiki.selfhtml.org/wiki/Robots.txt)) und berücksichtigt die Informationen beim Crawling. Wie kann ich eingeben, als welcher _UserAgent_ mein Browser auftritt?

5. Erweiterung 3: Gegebenenfalls müssen viele Unterseiten überprüft werden und es muss lange auf die Antwort der Anfragen gewartet werden. Informiert euch zu Parallelisierung in Python. Wie kann realisiert werden, dass nur eine feste Anzahl an Threads gleichzeitig arbeitet?

<span class="hidden-text">

index.html
- martina.mustermann@beispieldomain.de
- !#$%&’*+-/=?^_`.{|}~@example.com
- admin@[192.168.102.10]
- michaela-mustermann@beispieldomain.de
- hans-peter.richter@anwalt.beispielagentur.info
- farbenschubser@beispiel-agentur.de
1-unterseite.html
- x@example.com
- example-indeed@strange-example.com
- "john..doe"@example.org
1-1-unterseite (alles von 1-unterseite und zusätzlich:)
- fully-qualified-domain@example.com
- dmin@mailserver1
- user.name+tag+sorting@example.com
1-2-unterseite (alles von 1-unterseite und zusätzlich:)
- user-@example.org
- disposable.style.email.with+symbol@example.com
1-3-unterseite (alles von 1-unterseite und zusätzlich:)
- martina.mustermann@beispieldomain.de'>
2-unterseite.html
- 
2-kontakt.html
3-impressum.html

</span>

### Ausgangspunkte zur weiteren Recherche:

- Was sind gültige E-Mail-Adressen? Das Dokument "Request for comments 5322" legt [in Abschnitt 3.4.1](https://www.rfc-editor.org/rfc/rfc5322#section-3.4.1) fest, aus welchen Bestandteilen eine E-Mail-Adresse besteht und schlüsselt in [Abschnitt 3.2.3](https://www.rfc-editor.org/rfc/rfc5322#section-3.2.3) den Aufbau der Bestandteile auf.

- Wie erstelle ich [Reguläre Ausdrücke](https://oer-informatik.de/regex)?

